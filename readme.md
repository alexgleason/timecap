Timecap
=========

Timecap is an app that lets you share and eternalize moments. Users take a picture or write a story, and then place a Bottle containing the picture/story on the map. Users in the vicinity can uncap Bottles from the past and view the old stories/pictures placed by other users.


![Screenshot](img/screen4.png)

Team
----

[Alex Gleason](https://github.com/alexgleason)

[Pato Lankenau](https://github.com/pato)

[Will Yager](https://github.com/wyager)

Backend code [here](https://github.com/wyager/mhacks_messageinabottle_server).
