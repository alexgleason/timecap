package net.timecapapp.timecap;

import java.io.File;
import net.timecapapp.timecap.R;

import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DropBottleActivity extends Activity {
	private String mCurrentPhotoPath;
	private TextView message;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drop_bottle);
		
		message = (TextView)findViewById(R.id.messageBox);
		
		try{
			mCurrentPhotoPath = this.getIntent().getStringExtra("mCurrentPhotoPath");
			ImageView imageView = (ImageView) findViewById(R.id.photoView);
			
			//Bitmap image = BitmapFactory.decodeFile(mCurrentPhotoPath);
	        //imageView.setImageBitmap(image);
	        
	        // Get the dimensions of the View
	        int targetW = 100;//imageView.getWidth();
	        int targetH = 200;//imageView.getHeight();

	        // Get the dimensions of the bitmap
	        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	        bmOptions.inJustDecodeBounds = true;
	        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	        int photoW = bmOptions.outWidth;
	        int photoH = bmOptions.outHeight;

	        // Determine how much to scale down the image
	        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

	        // Decode the image file into a Bitmap sized to fill the View
	        bmOptions.inJustDecodeBounds = false;
	        bmOptions.inSampleSize = scaleFactor;
	        bmOptions.inPurgeable = true;

	        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	        imageView.setImageBitmap(bitmap);
		}catch (Exception e){
			throw new RuntimeException("Error [DropBottleActivity]! "+e);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.drop_bottle, menu);
		return true;
	}
	
	public void dropBtnPressed(View v){
		String messageTxt = message.getText().toString();
		Location location = MainActivity.me;
		File image = new File(mCurrentPhotoPath);
		
		MainActivity.bottles.addBottle(location.getLatitude(), location.getLongitude(), image, messageTxt);
		finish();
	}

}
