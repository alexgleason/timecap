package net.timecapapp.timecap;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class BottleRack implements Iterable<Map.Entry<String, Bottle>>{
	private HashMap<String, Bottle> rack;
	
	public BottleRack(){
		rack = new HashMap<String, Bottle>();
	}
	
	public void addBottle(double lattitude, double longitude, File image, String text){
		Bottle t = new Bottle(lattitude, longitude, image, text);
		rack.put(t.getID(), t);
		try{
			String data = URLEncoder.encode(Server.serialize(t), "UTF-8");//Server.serialize(t);//URLEncoder.encode(Server.serialize(t), "UTF-8");//t.getImage64();//Server.serialize(t);
			Log.d("DEBUG", "LENGTH OF SERIALIZED DATA TO SEND: "+data.length());
			Server.addBottle(t.getID(), lattitude, longitude, data);
		}catch (Exception e){
			Log.d("DEBUG", "DEATH!");
		}
	}
	
	public void addBottle(LatLng latlng, File image, String text){
		Bottle t = new Bottle(latlng, image, text);
		rack.put(t.getID(), t);
	}
	
	public void addBottle(Bottle b){
		rack.put(b.getID(), b);
	}
	
	public Bottle getBottle(String id){
		return rack.get(id);
	}

	@Override
	public Iterator<Entry<String, Bottle>> iterator() {
		return rack.entrySet().iterator();
	}


}
