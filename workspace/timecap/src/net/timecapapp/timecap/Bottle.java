package net.timecapapp.timecap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.UUID;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Base64;

import java.sql.Timestamp;
import com.google.android.gms.maps.model.LatLng;

public class Bottle implements Serializable{
	private static final long serialVersionUID = 13378008135L;
	private String id;
	private String text;
	private String image; // base64
	private String timestamp;
	private double lattitude;
	private double longitude;

	public Bottle(double lattitude, double longitude, File image, String text) {
		this.lattitude = lattitude;
		this.longitude = longitude;
		this.timestamp = new Timestamp(new java.util.Date().getTime()).toString();
		this.image = image==null?null:fileToBase64(image);
		this.text = text;
		this.id = generateID();
	}
	
	public Bottle(double lattitude, double longitude, String image) {
		this.lattitude = lattitude;
		this.longitude = longitude;
		this.timestamp = new Timestamp(new java.util.Date().getTime()).toString();
		this.image = image;
		this.text = "Placeholder text";
		this.id = generateID();
	}

	public Bottle(LatLng latlng, File image, String text) {
		this(latlng.latitude, latlng.longitude, image, text);
	}
	
//	public Bottle(double lattitude, double longitude, String text){
//		this(lattitude, longitude, null, text);
//	}

	public Bottle(Location location, File image, String text) {
		this(location.getLatitude(), location.getLongitude(), image, text);
	}

	public String getID() {
		return id;
	}

	public String getText() {
		return text;
	}
	
	public String getTimestamp(){
		return timestamp;
	}

	public String getImage64() {
		return image;
	}

	public Bitmap getImage() {
		byte[] imageAsBytes = Base64.decode(getImage64().getBytes(),
				Base64.DEFAULT);
		return BitmapFactory.decodeByteArray(imageAsBytes, 0,
				imageAsBytes.length);
	}

	public double getLattitude() {
		return lattitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public LatLng getLatLng() {
		return new LatLng(lattitude, longitude);
	}

	private String fileToBase64(File image) {
		Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
		byte[] b = baos.toByteArray();
		return Base64.encodeToString(b, Base64.DEFAULT);
	}
	
	private String generateID(){
		return ""+UUID.randomUUID().getMostSignificantBits();
	}
}
