package net.timecapapp.timecap;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import net.timecapapp.timecap.R;

public class ViewBottleActivity extends Activity {
	private boolean starOn = false;
	private String bottleID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_bottle);

		// Construct bottle var from intent extras!
		// Bottle bottle = new Bottle(0, 0, null, null);
		bottleID = this.getIntent().getStringExtra("bottleID");
		final Bottle bottle = MainActivity.bottles.getBottle(bottleID);

		RelativeLayout theLayout = (RelativeLayout) findViewById(R.id.activity_view_bottle);

		ImageView viewBottle = new ImageView(this);
		viewBottle.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		viewBottle.setImageBitmap(bottle.getImage());
		theLayout.addView(viewBottle);

		Toast.makeText(this, bottle.getTimestamp() + "\n" + bottle.getText(),
				Toast.LENGTH_LONG).show();

		viewBottle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(ViewBottleActivity.this,
						bottle.getTimestamp() + "\n" + bottle.getText(),
						Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_bottle, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.favorite:
			if (starOn) {
				item.setIcon(R.drawable.btn_star_off_normal_holo_dark);
				removeFromFavorites();
				starOn = false;
			} else {
				item.setIcon(R.drawable.btn_star_on_normal_holo_dark);
				addToFavorites();
				starOn = true;
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void addToFavorites() {
		// TODO
	}

	private void removeFromFavorites() {
		// TODO
	}
}
