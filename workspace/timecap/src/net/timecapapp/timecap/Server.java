package net.timecapapp.timecap;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URLDecoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import com.loopj.android.http.*;

public class Server {
	private static final double bottleRadius = 0.5;
	public static final String serverAddr = "http://timecap.alexgleason.me:8005";
	public static String result;

	public static void getBottles(double lat, double lng) {
		Log.d("DEBUG", "getBottles");
		makeRequest("/getat/" + lat + "/" + lng + "/" + bottleRadius, true);
	}

	public static void dealWithBottles(String response) {
		// Log.d("DEBUG", "dealWithBottles: "+response);
		int bottles = 0;
		for (int i = 0; i < response.length(); i++) {
			if (response.charAt(i) == '{') {
				bottles++;
			}
		}
		int start = 0;
		for (int i = 0; i < bottles; i++) {
			int index = response.indexOf("\"ID\"", start);
			String ID = response.substring(index + 6,
					response.indexOf("\"", index + 6));
			index = response.indexOf("\"lat\"", start);
			String latstring = response.substring(index + 6,
					response.indexOf("\"", index + 6)).replaceAll(",", "");
			index = response.indexOf("\"lon\"", start);
			String lonstring = response.substring(index + 6,
					response.indexOf("\"", index + 6)).replaceAll(",", "");
			index = response.indexOf("\"data\"", start);
			String data = response.substring(index + 8,
					response.indexOf("\"", index + 8));
			start = index + 8;
			try {
				data = URLDecoder.decode(data, "UTF-8");
			} catch (Exception e) {
			}
			;
			//Bottle bb = new Bottle(Double.parseDouble(latstring), Double.parseDouble(lonstring), data);
			Bottle bb = Server.deserialize(data);
			bb = bb==null?new Bottle(Double.parseDouble(latstring), Double.parseDouble(lonstring), data):bb;
			MainActivity.bottles.addBottle(bb);
			MainActivity.updateBottles();
			Log.d("DEBUG", "ADDED BOTTLE!");
		}
	}

	public static void addBottle(String id, double lat, double lng, String data) {
		// makeRequest("/put/" + id + "/" + lat + "/" + lng + "/" + data,
		// false);

		RequestParams params = new RequestParams();
		params.put("opcode", "put");
		params.put("id", id);
		params.put("lat", Double.valueOf(lat).toString());
		params.put("lng", Double.valueOf(lng).toString());
		params.put("data", data);

		makePostRequest(params);
	}

	public static void makePostRequest(RequestParams params) {
		AsyncHttpClient client = new AsyncHttpClient();
		client.post(serverAddr, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				Log.d("DEBUG", "POST Thread Success! Response: " + response);

				result = response;
			}
		});
	}

	public static void makeRequest(String data, final boolean getBottles) {
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(serverAddr + data, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
				Log.d("DEBUG", "GET Thread Success! Response: " + response);
				Log.d("DEBUG", "RESPONSE LENGTH: " + response.length());
				result = response;
				if (getBottles) {
					dealWithBottles(response);
				}
			}
		});
	}

	public static String convertStreamToString(InputStream inputStream)
			throws IOException {
		if (inputStream != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(
						inputStream, "UTF-8"), 1024);
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				inputStream.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	public static Bottle deserialize(String serialText) {
		Bottle bottle;
		try {
			InputStream is = new ByteArrayInputStream( serialText.getBytes());
			ObjectInputStream in = new ObjectInputStream(is);
			bottle = (Bottle) in.readObject();
			in.close();
			is.close();
		} catch (IOException i) {
			i.printStackTrace();
			bottle = null;
		} catch (ClassNotFoundException c) {
			System.out.println("Employee class not found");
			c.printStackTrace();
			bottle = null;
		}
		return bottle;
	}

	public static String serialize(Bottle bottle) {
		String result = "";
		ObjectOutput out = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(bottle);
			byte[] yourBytes = bos.toByteArray();
			result = new String(yourBytes);
		} catch (Exception e) {
			throw new RuntimeException("Error serializing data " + e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
		return result;
	}
}

class MakeRequest extends AsyncTask<String, Void, String> {

	private Exception exception;

	protected String doInBackground(String... urls) {
		String result = "";
		Log.d("DEBUG", "Started async thread");
		try {
			HttpClient client = new DefaultHttpClient();

			String data = urls[0];
			Log.d("DEBUG", data);
			HttpGet request = new HttpGet(Server.serverAddr + data);
			Log.d("DEBUG", Server.serverAddr);
			HttpResponse response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				result += line + "\n";
				Log.d("DEBUG", "reading line: " + line);
			}
			Log.d("DEBUG", "server response: " + result);
		} catch (Exception e) {
			this.exception = e;
			return null;
		}
		return result;
	}

	protected void onPostExecute(String result) {
		// TODO: check this.exception
		// TODO: do something with the feed
	}
}