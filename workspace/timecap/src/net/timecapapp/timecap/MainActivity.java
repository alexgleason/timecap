package net.timecapapp.timecap;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.timecapapp.timecap.R;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.*;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity implements LocationListener,
		OnMarkerClickListener {
	public static BottleRack bottles;
	public static Location me = null;

	private static HashMap<String, String> markerToBottle = new HashMap<String, String>();

	private LocationManager locationManager;
	private String provider;
	private static GoogleMap map;
	public String mCurrentPhotoPath;

	static final int REQUEST_IMAGE_CAPTURE = 1;
	private static BitmapDescriptor markerBitmap;
	private int proximityThreshold = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set up bottle rack
		bottles = new BottleRack();

		// Get a handle to the Map Fragment
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();

		map.setMyLocationEnabled(true);

		// Load the marker bitmap
		markerBitmap = BitmapDescriptorFactory.fromResource(R.drawable.bottle);

		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// Define the criteria how to select the location provider -> use
		// default
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		Location location = locationManager.getLastKnownLocation(provider);

		// Initialize the location fields
		if (location != null) {
			onLocationChanged(location);
			me = location;
		} else {
			Toast.makeText(this, "Error: location not availailable",
					Toast.LENGTH_SHORT).show();
		}

		map.setOnMarkerClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.new_bottle:
			addBottleButton();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static void updateBottles() {
		// clear the map
		map.clear();
		for (Map.Entry<String, Bottle> bottle : bottles) {
			Bottle b = bottle.getValue();
			dropBottle(b.getText(), b.getLattitude(), b.getLongitude(),
					b.getID());
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			startActivity(new Intent(this, DropBottleActivity.class).putExtra(
					"mCurrentPhotoPath", mCurrentPhotoPath));
		}
	}

	private void addBottleButton() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
			}
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
			} else {
				throw new RuntimeException("Failed to create image file");
			}
		}
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(imageFileName, /* prefix */
				".jpg", /* suffix */
				storageDir /* directory */
		);

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = image.getAbsolutePath();
		return image;
	}

	private static void dropBottle(String message, double lat, double lng,
			String bottleID) {
		LatLng t = new LatLng(lat, lng);
		Marker marker = map.addMarker(new MarkerOptions().position(t).icon(
				markerBitmap));
		markerToBottle.put(marker.getId(), bottleID);
	}

	/* Request updates at startup */
	@Override
	protected void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(provider, 400, 1, this);
		updateBottles();
	}

	/* Remove the location listener updates when Activity is paused */
	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}

	@Override
	public void onLocationChanged(Location location) {
		double lat = (location.getLatitude());
		double lng = (location.getLongitude());

		Server.getBottles(lat, lng);

		me = location;

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(
				new LatLng(location.getLatitude(), location.getLongitude()), 15));
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		Toast.makeText(this, "Enabled new provider " + provider,
				Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(this, "Disabled provider " + provider,
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		marker.getId();
		float[] distance = new float[1];
		Location.distanceBetween(me.getLatitude(), me.getLongitude(),
				marker.getPosition().latitude, marker.getPosition().longitude,
				distance);
		if (distance[0] <= proximityThreshold) {
			String id = markerToBottle.get(marker.getId());
			Bottle b = bottles.getBottle(id);
			startActivity(new Intent(this, ViewBottleActivity.class).putExtra(
					"bottleID", id));
			return true;
		} else {
			Toast.makeText(this, "Bottle too far to open! Try getting closer.",
					Toast.LENGTH_SHORT).show();
		}
		return false;
	}
}